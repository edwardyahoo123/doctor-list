import React from 'react';
import styled from 'styled-components';

export const StyledMainContainer = styled.div`
    max-width: 900px;
    margin: 0 auto;

    @media (max-width: 576px) {
        padding: 0 20px;
    }

    .tags__wrapper {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        margin: 15px 0;
        
        .tag {
            border-style: solid;
            border-width: 1px;
            border-color: #ff0168;
            border-radius: 20px;
            color: #ff0168;
            cursor: pointer;
            display: inline-block;
            padding: 5px 15px;
            margin-right: 5px;
            margin-bottom: 5px;
            
            span:before {
                content: '✖ ';
            }
        }
    }
`

export const StyleTagsWrapper = styled.div`
    display: flex;
    flex-direction: row;
    margin: 15px 0;
`

export const Tags = styled.div`
    display: inline-block;
    margin-right: 5px;
    padding: 5px 15px;
    border-style: solid;
    border-width: 1px;
    border-color: #d0d0da;
    border-radius: 20px;
`