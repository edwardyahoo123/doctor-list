import React, { useEffect, useContext, useCallback } from 'react';
import csvtojson from 'csvtojson';

import { StyledMainContainer } from './Main.style';
import { GlobalStateContext } from '../../hook/GlobalContext';
import { ACTION_TYPES } from '../../hook/Reducer';
import DoctorCard from '../../components/DoctorCard/DoctorCard';
import Select from '../../components/Select/Select';

const Main = () => {
    const { dispatch, regions, displayDoctors, filterTags } = useContext(GlobalStateContext);

    useEffect(() => {
        const fetchCSV = async () => {
            try {
                const res = await fetch('/doctors.csv');
                const resText = await res.text()
                const data = await csvtojson().fromString(resText);
                dispatch({ type: ACTION_TYPES.STORE_DOCTOR_LIST, data })
            } catch (e) {
                console.error(e);
            }
        }

        fetchCSV();
    }, [dispatch]);

    const handleRemoveTag = useCallback(
        (tag) => {
            dispatch({ type: ACTION_TYPES.UPDATE_FILTER_OPTION, data: { value: tag.value, name: tag.name } });
        },
        [dispatch],
    )

    console.log('regions', regions)

    return (
        <StyledMainContainer>
            <Select placeholder="Please select Location" data={regions} name="Location" />
            {
                filterTags && filterTags.length > 0 &&
                <div className="tags__wrapper">
                    {
                        filterTags.map((tag, idx) => (
                            <div className="tag" key={idx} onClick={() => handleRemoveTag(tag)}>
                                <span>{tag.value}</span>
                            </div>
                        ))
                    }
                </div>
            }
            <div className="doctor__cards__wrapper">
                {displayDoctors.map((doct, idx) => (
                    <DoctorCard key={doct['Doctor ID'] + idx} {...doct} />
                ))}
            </div>
        </StyledMainContainer>
    )
}

export default Main;