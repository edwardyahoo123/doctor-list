import React from 'react';
import styled from 'styled-components';

export const StyledDoctorCardWrapper = styled.div`
    display: inline-block;
    width: 50%;
    vertical-align: middle;
    padding: 15px 15px;

    @media (max-width: 576px) {
        width: 100%;
        padding: 20px 0;
    }
    
    .doctor__card {
        background: #fbfbfb;
        min-height: 227px;
        border-radius: 10px;
        padding: 20px 20px 10px;

        .doctor__name {
            font-size: 15px;
            margin-bottom: 0;
            margin-top: 0;
            line-height: 20px;
        }

        .doctor__chip__wrapper {
            margin-top: 10px;

            .doctor__chip {
                border-style: solid;
                border-width: 1px;
                border-color: #d0d0da;
                border-radius: 20px;
                display: inline-block;
                margin-right: 5px;
                padding: 5px 15px;
            }
        }
        
        .doctor__location {
            margin-top: 15px;
            /* padding-left: 15px; */
            color: #1d1f45;
            font-size: 15px;
            line-height: 1.6;
            text-decoration: none;
        }

        .doctor__location:before {
            content: '🚩'
        }

        .doctor__phone {
            opacity: .6;
            font-size: 12px;
            line-height: 1.67;
        }

        .doctor__details {
            position: static;
            display: inline;
            overflow: visible;
            color: #707188;
            font-size: 12px;

            .doctor__day:not(:last-child):after {
                content: '; '
            }
        }
    }
`