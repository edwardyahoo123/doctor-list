import React from 'react';
import { StyledDoctorCardWrapper } from './DoctorCard.style';

const DoctorCard = React.memo((props) => {
    return (
        <StyledDoctorCardWrapper>
            <div className="doctor__card">
                <div className="doctor__name">{props.ChiName}</div>
                {
                    props && props['Service Type'] && (
                        <div className="doctor__chip__wrapper">
                            <div className="doctor__chip">
                                {props['Service Type']}
                            </div>
                        </div>
                    )
                }
                <a className="doctor__location" href={`https://maps.google.com/?q=${props.ChiAddress2}`}>{props.ChiAddress2}</a>
                <div className="doctor__phone">{props.Telephone1}</div>
                <div className="doctor__details">
                    {
                        [1, 2, 3, 4].map(it => {
                            if (props['ChiDay Seq ' + it] !== '') {
                                return (
                                    <span className="doctor__day">
                                        {props['ChiDay Seq ' + it]}: {props['ChiDay Seq ' + it + ' time']}
                                    </span>
                                )
                            }

                            return null;
                        })
                    }
                </div>
            </div>
        </StyledDoctorCardWrapper>
    )
})

export default DoctorCard;