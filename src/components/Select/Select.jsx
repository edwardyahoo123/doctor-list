import React, { useCallback, useState, useEffect, useContext } from 'react';

import { StyledSelectWrapper } from './Select.style';
import { ACTION_TYPES } from '../../hook/Reducer';
import { GlobalStateContext } from '../../hook/GlobalContext';
import MouseDownListener from '../../shared/MouseDownListener';

const Select = ({ placeholder, data, name }) => {
    const [openMenu, setOpenMenu] = useState(false);
    const [selectOptions, setSelectOptions] = useState(null);
    const { dispatch } = useContext(GlobalStateContext);

    useEffect(() => {
        if (data) {
            const options = {}
            Object.keys(data).forEach(key => {
                options[key] = {
                    data: data[key],
                    isOpen: false
                };
            })

            setSelectOptions(options)
        }
    }, [data]);

    const handleClickMenu = useCallback(() => {
        setOpenMenu(prev => !prev);
    }, [setOpenMenu]);

    const handleClickSubmenu = useCallback((value) => {
        setSelectOptions(
            prevOpts => {
                if (prevOpts[value].isOpen) {
                    prevOpts[value].isOpen = false;
                } else {
                    Object.keys(prevOpts).forEach(key => {

                        prevOpts[key].isOpen = key === value;
                    });
                }

                return {
                    ...prevOpts
                };
            }
        )
    }, [setSelectOptions]);

    const handleOnBlur = useCallback((e) => {
        setOpenMenu(false);
    }, [setOpenMenu]);

    const handleSelectOption = useCallback((value) => {
        dispatch({ type: ACTION_TYPES.UPDATE_FILTER_OPTION, data: { value, name } });
        setOpenMenu(false);
    }, [dispatch, name]);

    const handleClickEvent = useCallback((path) => {
        if (!path.some(p => p.className === 'drop-down__menu')) {
            setOpenMenu(false);
        }
    }, []);

    return (
        <StyledSelectWrapper>
            <div className="select__box" onClick={handleClickMenu}>
                <span>
                    {placeholder}
                </span>
            </div>
            {
                openMenu &&
                <div className="drop-down__menu" onBlur={handleOnBlur} tabIndex="1">
                    <MouseDownListener callback={handleClickEvent}/>
                    {
                        selectOptions && Object.keys(selectOptions).map((key, idx) => (
                            <div key={idx}>
                                <div className="drop-down__menu__item" onClick={() => handleClickSubmenu(key)}>{key}</div>
                                {
                                    selectOptions[key].isOpen &&
                                    <div className="drop-down__submenu">
                                        {
                                            selectOptions && selectOptions[key].data.map((opt, optIdx) => (
                                                <>
                                                    <div className="drop-down__submenu__item" key={optIdx} onClick={() => handleSelectOption(opt)}>{opt}</div>
                                                </>
                                            ))
                                        }
                                    </div>
                                }
                            </div>

                        ))
                    }
                </div>
            }
        </StyledSelectWrapper>
    )
};

export default Select;