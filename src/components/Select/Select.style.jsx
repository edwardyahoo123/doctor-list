import React from 'react';
import styled from 'styled-components';

export const StyledSelectWrapper = styled.div`
    cursor: pointer;
    position: relative;

    .select__box {
        width: 100%;
        margin-top: 30px;
        padding: 14px 50px 14px 14px;
        border-radius: 5px;
        background-color: #eef1f9;
        position: relative
    }

    .drop-down__menu {
        border-radius: 5px;
        padding-top: 5px;
        padding-bottom: 25px;
        position: absolute;
        top: 100%;
        left: 0;
        width: 100%;
        z-index: 90;
        margin-top: 5px;
        background-color: #f7f8fb;

        &__item {
            color: #505051;
            font-size: 13px;
            padding: 5px 10px;
        }
    }

    .drop-down__submenu {
        background: #DDDDDD;
        padding-left: 10px;
        
        &__item {
            color: #ff0168;
            padding: 15px 10px;
        }
    }
`
export const StyledDropDownMenu = styled.div`
    background-color: #f7f8fb;
    padding-top: 5px;
    padding-bottom: 25px;
    position: absolute;
`