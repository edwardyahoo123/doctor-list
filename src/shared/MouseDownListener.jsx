import React, { useEffect } from 'react';

const MouseDownListener = React.memo(({ callback }) => {
    useEffect(() => {
        const handleMouseDown = event => {
            console.log(event);
            console.log(event.target.className);
            callback(event.path);
        }

        document.addEventListener('mouseup', handleMouseDown);
        return () => {
            document.removeEventListener('mouseup', handleMouseDown);
        };
    }, [callback]);

    return (
        <></>
    )
});

export default MouseDownListener;