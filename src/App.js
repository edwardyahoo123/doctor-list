import React from 'react';

import './App.css';
import { GlobalStateContextProvider } from './hook/GlobalContext';
import Main from './pages/main/Main';

function App() {
  return (
    <GlobalStateContextProvider>
      <Main/>
    </GlobalStateContextProvider>
  );
}

export default App;
