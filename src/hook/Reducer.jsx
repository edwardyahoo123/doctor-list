import React from 'react';

export const ACTION_TYPES = {
    STORE_DOCTOR_LIST: 'STORE_DOCTOR_LIST',
    UPDATE_FILTER_OPTION: 'UPDATE_FILTER_OPTION'
};

export function reducer(state, action) {
    switch (action.type) {
        case ACTION_TYPES.STORE_DOCTOR_LIST: {
            if (action.data && action.data.length > 0) {
                const regions = action.data.map(doct => doct.Region).filter((region, idx, regions) => regions.indexOf(region) === idx);
                state.regions = {}

                regions.forEach(region => {
                    state.regions[region] = [];
                })

                action.data.forEach(doct => {
                    if (doct.Region && state.regions[doct.Region].indexOf(doct.Location) < 0) {
                        state.regions[doct.Region].push(doct.Location);
                    }
                })
                console.log(state)
                state.doctors = action.data;
            }
            break;
        }
        case ACTION_TYPES.UPDATE_FILTER_OPTION: {
            const { value, name } = action.data;
            console.log('filter tag', state.filterTags)
            if (state.filterTags.find(ftag => ftag.name === name && ftag.value === value)) {
                // Remove filter
                state.filterTags = state.filterTags.filter(tag => tag.value !== value || tag.name !== name);
            } else {
                // Add filter
                state.filterTags.push({ value, name });
            }

            // Update display doctor list
            state.displayDoctors = state.doctors.filter(doct => {
                return state.filterTags.some(ftag => ftag.value === doct.Location);
            });
            console.log('state', state)
            break;
        }
        default: {
            break;
        }
    }

    return {
        ...state
    }
}