import React, { useReducer } from 'react';
import { reducer } from './Reducer';

const initValues = {
    doctors: [],
    displayDoctors: [],
    filterTags: [],
    regions: {}
}

export const GlobalStateContext = React.createContext(initValues);

export const GlobalStateContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initValues);

    return (
        <GlobalStateContext.Provider value={{
            dispatch,
            doctors: state.doctors,
            displayDoctors: state.displayDoctors,
            filterTags: state.filterTags,
            regions: state.regions
        }}>
            {children}
        </GlobalStateContext.Provider>
    )
}